This package is a selection of tools useful for pre- and postprocessing of snowpack simulations. Check out their individual READMEs.

# caaml
A tool to prepare CAAMLv6 snow profile files to initialize Snowpack simulations. It checks for required parameters (temperature, grain size, ...), parameterizes a density profile if required and sets required/optional metadata for the simmulation.

-> [caaml](https://gitlab.com/avalanche-warning/snow-cover/postprocessing/snowpacktools/-/tree/main/snowpacktools/caaml)

# SNOWPRO
A processing tool to parse .pro files and visualize the snowpack evolution. Avapro is based on the datastructure provided through snowpro.

<p float="center">
  <img src="./snowpacktools/snowpro/figures/WFJ-AWS_snp_evo_Punstable.png" width="85%" />
</p>

-> [snowpro](https://gitlab.com/avalanche-warning/snow-cover/postprocessing/snowpacktools/-/tree/main/snowpacktools/snowpro)

# AVAPRO
Assessment and Validation of Avalanche PROblems. An automated characterization of avalanche problems utilizing snow cover simulations.

<p float="center">
  <img src="./snowpacktools/avapro/figures/snowprofile18284_sk38.png" width="85%" />
</p>

-> [avapro](https://gitlab.com/avalanche-warning/snow-cover/postprocessing/snowpacktools/-/tree/main/snowpacktools/avapro)

# AGGREGATEPRO
A tool to compute a representative profile from a larger group of profiles and visualize layer stability distributions.

<p float="center">
  <img src="./snowpacktools/aggregatepro/figures/tsstab.png" width="85%" />
</p>

-> [aggregatepro](https://gitlab.com/avalanche-warning/snow-cover/postprocessing/snowpacktools/-/tree/main/snowpacktools/aggregatepro)