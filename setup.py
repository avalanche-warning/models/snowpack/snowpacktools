import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="snowpacktools",
    version="0.0.1",
    license="AGPL",
    author="Avalanche Warning Service Tyrol",
    author_email="lawine@tirol.gv.at",
    description="Pre- and postprocessing tools of snowpack simulations.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/avalanche-warning/models/snowpack/snowpacktools",
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
    install_requires=[
        "lxml",
        "numpy",
        "matplotlib",
        "pandas",
        "geopandas",
        "xarray",
        "netcdf4",
        "sympy",
        "joblib",
        "scikit-learn"
    ]
)
