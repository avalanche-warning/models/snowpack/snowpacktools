################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
# import sys
import time
# import configparser

import numpy as np
# import pandas as pd
# import xarray # needed for time axis
from datetime import datetime, timedelta
import joblib

import matplotlib.pyplot as plt
from matplotlib.colors import BoundaryNorm, ListedColormap
from matplotlib.ticker import AutoMinorLocator, FuncFormatter
from matplotlib.dates import DateFormatter
import matplotlib.image as image
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

from snowpacktools.snowpro import snowpro, pro_helper, instability_rfm_mayer


def plot_aps_and_profile_evolution(df_P, path_to_pro, DATETIME_STR=None, output_path='output/', var='grain_type', res='1h', second_var='NONE', COLOR_SCHEME='IACS2',DATE_RANGE=['NONE','NONE']):
    """Visualization of avalanche problems and snowpack evolution in one figure with two axes.
    Plots snowpack evolution (PRO-file). Different variables or grain type can be visualized and overlayed.
    
    Arguments:
        path_to_pro (str):      Path to PRO file to be plotted
        output_dir (str):     
        DATETIME_STR (str):
        var: optional           Can be used for visualizing other variables than grain type
        res (str):
        second_var (str):
        COLOR_SCHEME (str):
        DATE_RANGE (list):
    Returns:
        Figure
    """

    ### Load mpl style
    this_dir, this_filename = os.path.split(__file__)
    latex_template_path = os.path.join(this_dir, "../snowpro/latex_template.mplstyle")
    if os.path.exists(latex_template_path):
        plt.style.use(latex_template_path)

    ### Load icons of APs
    icon_path       = "graphics/icons-aps-EAWS-colour/jpg"
    icon_newsnow    = os.path.join(this_dir, icon_path, 'Icon-Avalanche-Problem-New-Snow-EAWS.jpg')
    icon_newsnow    = image.imread(icon_newsnow)
    icon_drift      = os.path.join(this_dir, icon_path, 'Icon-Avalanche-Problem-Wind-Drifted-Snow-EAWS.jpg')
    icon_drift      = image.imread(icon_drift)
    icon_persistent = os.path.join(this_dir, icon_path, 'Icon-Avalanche-Problem-Persistent-Weak-Layer-EAWS.jpg')
    icon_persistent = image.imread(icon_persistent)
    icon_deep_pwl   = os.path.join(this_dir, icon_path, 'Icon-Avalanche-Problem-Deep-Persistent-Weak-Layer-EAWS.jpg')
    icon_deep_pwl   = image.imread(icon_deep_pwl)
    icon_wet        = os.path.join(this_dir, icon_path, 'Icon-Avalanche-Wet-Snow-EAWS.jpg')
    icon_wet        = image.imread(icon_wet)
    icon_gliding    = os.path.join(this_dir, icon_path, 'Icon-Avalanche-Problem-Gliding-Snow-EAWS.jpg')
    icon_gliding    = image.imread(icon_gliding)
    icon_list = [icon_newsnow, icon_drift, icon_persistent, icon_deep_pwl, icon_wet, icon_gliding]

    start_time = time.time()
    profs, meta_dict = snowpro.read_pro(path_to_pro, res=res, keep_soil=False, consider_surface_hoar=True)

    # Filter for certain resolution and time frame
    w, _ = pro_helper.set_resolution(res)
    LABELS_GRAIN_TYPE, COLORS_GRAIN_TYPE, HATCHES_GRAIN_TYPE, LABELS_GRAIN_TYPE_BAR, COLORS_GRAIN_TYPE_BAR, HATCHES_GRAIN_TYPE_BAR = pro_helper.get_grain_type_colors(COLOR_SCHEME)
    RANGE_DICT = pro_helper.get_range_dict()

    """Color map and preprocessing"""
    rta = 0.75
    if var=='grain_type':
        col_dict_labels     = dict(zip(LABELS_GRAIN_TYPE, COLORS_GRAIN_TYPE))
        hatches_dict_labels = dict(zip(LABELS_GRAIN_TYPE, HATCHES_GRAIN_TYPE))

        n_bar = len(LABELS_GRAIN_TYPE_BAR)
        col_nums = np.arange(0,n_bar)
        col_dict = dict(zip(col_nums, COLORS_GRAIN_TYPE_BAR[::-1]))
        cmap = ListedColormap([col_dict[x] for x in col_dict.keys()])
    else:
        if var == 'Punstable':
            profs     = instability_rfm_mayer.calc_punstable(profs)
            cmap_var  = pro_helper.get_Punstable_cmap()
            var_ticks = np.arange(0,1.1,0.1)
            var_label = var + " (RTA > 0.75)"
        else:
            if var == 'Sk38' or var == 'Sn38':
                var_label = var + " (RTA > 0.75)"
                cmap_var  = pro_helper.get_sk38_cmap()
                var_ticks = np.arange(0,1.6,0.1)
            else:
                cmap_var  = plt.get_cmap('plasma_r')
                var_label = var
        clev_var  = np.linspace(RANGE_DICT[var][0],RANGE_DICT[var][1],100) # 11
        # cnorm_var = BoundaryNorm(boundaries=clev_var, ncolors=cmap_var.N, clip=True)

    plot_second_var  = False
    var_alpha        = 1
    second_vars      = ['Sk38','Sn38','Punstable']
    hatch_second_var = ''
    if second_var in second_vars:
        plot_second_var  = True
        second_var_label = second_var + " (RTA > 0.75)"
        #var_alpha        = 1 #cmb
        var_alpha        = 0.33
        if second_var == 'Sk38' or second_var == 'Sn38':
            cmap_var2        = pro_helper.get_sk38_cmap()
            second_var_ticks = np.arange(0,1.6,0.1)
        elif second_var == 'Punstable':
            profs            = instability_rfm_mayer.calc_punstable(profs)
            cmap_var2        = pro_helper.get_Punstable_cmap()
            second_var_ticks = np.arange(0,1.1,0.1)
        else:
            # cmap_var2  = plt.get_cmap('gist_gray')
            cmap_var2 = pro_helper.get_whiteout_cmap(reverse=True)
            
        clev_var2 = np.linspace(RANGE_DICT[second_var][0],RANGE_DICT[second_var][1],100) # 11 discrete colorbar
        # cnorm_var2 = BoundaryNorm(boundaries=clev_var2, ncolors=cmap_var2.N, clip=False)

    # VISUALIZATION
    fig, ((ax0, ax),(ax1,ax_aps)) = plt.subplots(2,2,figsize=(11,7),sharex=True, gridspec_kw={'width_ratios':[1,15],'height_ratios':[5,4],'hspace':0.03,'wspace':0.03}) # 'height_ratios':[1,1]
    ax0.axis('off')
    ax1.axis('off')

    h_max = []
    dates = []
    for i, ts in enumerate(profs):
        prof = profs[ts]
        dates.append(ts)
        if len(prof['height']>0):
            h_max.append(prof['height'][-1])
                
            if var=='grain_type':
                cols   =[]
                hatches=[]
                for row in range(0,len(prof['graintype'])):
                    cols.append(col_dict_labels[prof['graintype'][row][0]])
                    hatches.append(hatches_dict_labels[prof['graintype'][row][0]])
                ax.bar(ts, prof['thickness'], width=w, bottom=prof['bottom'], align='edge', color=cols, hatch=hatches, alpha=var_alpha) # label=labels[i])
            else:
                thickness = np.where(prof['RTA']>=rta, prof['thickness'], np.nan)
                bottom    = np.where(prof['RTA']>=rta, prof['bottom'],    np.nan)

                var_data = (prof[var]-RANGE_DICT[var][0])/(RANGE_DICT[var][1]-RANGE_DICT[var][0])
                cols     = cmap_var(var_data)
                ax.bar(ts, prof['height'][-1], width=w, bottom=0, align='edge', color='lightgrey', alpha=1)
                ax.bar(ts, thickness, width=w, bottom=bottom, align='edge', color=cols, alpha=1)
                
            
            if plot_second_var:
                """Filter data with RTA"""
                thickness = np.where(prof['RTA']>=rta, prof['thickness'], np.nan)
                bottom    = np.where(prof['RTA']>=rta, prof['bottom'],    np.nan)
                hatches   = np.where(prof['RTA']>=rta, hatch_second_var, np.nan)

                var_data2 = np.where(prof['RTA']>=rta, prof[second_var],  np.nan)
                var_data2 = (var_data2-RANGE_DICT[second_var][0])/(RANGE_DICT[second_var][1]-RANGE_DICT[second_var][0])
                cols2 = cmap_var2(var_data2)
                ax.bar(ts, thickness, width=w, bottom=bottom, align='edge', color=cols2, hatch=hatches, alpha=1)
                #ax.bar(ts, thickness, width=w, bottom=bottom, align='edge', color=cols2, hatch=hatches, alpha=0) #cmb
        else:
            h_max.append(0)

    """Line along snow surface"""
    ax.plot(dates,h_max,ds='steps-post',lw=0.8,color='black', ls='--', alpha=0.67)

    """COLORBAR (Norm, bins, formatter, ticks - lots of stuff to make colorbar look nice)"""
    ## !!!ax0 is used and fraction changed compared to snowpro plot for alignment with ava problems!!!
    if second_var!='NONE':
         # - Colorbar for second layer - # 
        n_var = 9
        lulu = np.zeros((n_var,n_var))
        for nn in range(0,n_var):
            lulu[nn, :] = np.nan # nn
        contf = ax.contourf(lulu, cmap=cmap_var2, levels=clev_var2, hatches=hatch_second_var)
        cbar2 = fig.colorbar(contf,ax=ax0, location='left', ticks=second_var_ticks, fraction=1) # shrink=0.7
        cbar2.set_label(second_var_label)

        """Modify grain type legend for SARPGR"""
        LABELS_GRAIN_TYPE  = ['PP(gp), DF','SH, DH','FC(xr), RG','MF(cr), IF']
        COLORS_GRAIN_TYPE  = ['#ffde00','#95258f','#dacef4','#d5ebb5']
        HATCHES_GRAIN_TYPE = ['','','','']
        pro_helper.add_custom_legend(ax, LABELS_GRAIN_TYPE, COLORS_GRAIN_TYPE, HATCHES_GRAIN_TYPE, x=0.2, y=1.03, width=0.028, height=0.025, spacing=0.15, alpha=var_alpha)
        #pro_helper.add_custom_legend(ax, LABELS_GRAIN_TYPE[1:], COLORS_GRAIN_TYPE[1:], HATCHES_GRAIN_TYPE[1:], x=0.015, y=1.03, width=0.028, height=0.025, spacing=0.092, alpha=var_alpha) #cmb

    else:
        if var=='grain_type':
            pro_helper.add_custom_legend(ax, LABELS_GRAIN_TYPE[1:], COLORS_GRAIN_TYPE[1:], HATCHES_GRAIN_TYPE[1:], x=0.015, y=1.03, width=0.028, height=0.025, spacing=0.092, alpha=var_alpha)
        else:
            n_var = 9
            lulu = np.zeros((n_var,n_var))
            for nn in range(0,n_var):
                lulu[nn, :] = np.nan # nn
            contf = ax.contourf(lulu,cmap=cmap_var,levels=clev_var) #extend='max'
            cbar = fig.colorbar(contf,ax=ax0, location='left', ticks=var_ticks, fraction=1) 
            cbar.set_label(var_label)

            """Modify grain type legend for SARPGR"""
            LABELS_GRAIN_TYPE  = ['PP(gp), DF','SH, DH','FC(xr), RG','MF(cr), IF']
            COLORS_GRAIN_TYPE  = ['#ffde00','#95258f','#dacef4','#d5ebb5']
            HATCHES_GRAIN_TYPE = ['','','','']
            pro_helper.add_custom_legend(ax, LABELS_GRAIN_TYPE, COLORS_GRAIN_TYPE, HATCHES_GRAIN_TYPE, x=0.2, y=1.03, width=0.028, height=0.025, spacing=0.15, alpha=var_alpha)
    
    """Current timestamp (split nowcast and forecast)"""
    if DATETIME_STR!=None:
        datetime_format  = '%Y-%m-%dT%Hh%M'
        datetime_tmr     = datetime.strptime(DATETIME_STR, datetime_format) + timedelta(days=1)
        ax.axvline(x=datetime_tmr,ymin=-0.1, ymax=1.1, color='black', lw=1.5, ls='--')
        ax_aps.axvline(x=datetime_tmr,ymin=-0.1, ymax=1.1, color='black', lw=1.5, ls='--')
        ### datetime_tmr_txt = datetime_tmr + timedelta(hours=12)
        ### y_txt            = (np.max(h_max)+0.1) * 0.99
        ### ax.text(datetime_tmr_txt, y_txt, r"$\rightarrow$" + "\nForecast\n"+r"$\rightarrow$", horizontalalignment='left', verticalalignment='top')

    """Axes and labels"""
    if DATE_RANGE[0] == 'NONE':
        ax.set_xlim(dates[0],dates[-1])
    else:
        DATETIME_FORMAT  = '%Y-%m-%d' # +01:00
        d0 = datetime.strptime(DATE_RANGE[0], DATETIME_FORMAT)
        d1 = datetime.strptime(DATE_RANGE[1], DATETIME_FORMAT)
        ax.set_xlim(d0,d1)

    ax.set_ylim(0,1.1*np.max(h_max))

    if second_var!='NONE' or var!="grain_type":
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position("right")
    else:
        ax.yaxis.tick_left()
        ax.yaxis.set_label_position("left")
    ax.set_ylabel("height / cm")
    date_fmt = DateFormatter("%b-%d")
    ax.xaxis.set_major_formatter(date_fmt)
    ax.yaxis.set_minor_locator(AutoMinorLocator())

    """Include Meta data in top left corner and save figure"""
    meta_x = 0.015
    meta_y = 0.975
    header_str   = 'Location:' + '\nElevation:' + '\nSlope Angle:' + '\nAspect:'
    header_str_2 = meta_dict['StationName'] + '\n' + meta_dict['Altitude'] + 'm\n' + str(int(float(meta_dict['SlopeAngle']))) + '°\n' + str(int(float(meta_dict['SlopeAzi'])))  + '°'
    ax.text(meta_x,        meta_y, header_str,   horizontalalignment='left', verticalalignment='top', transform=ax.transAxes, fontsize=10) # ma='left'
    ax.text(meta_x + 0.1, meta_y, header_str_2, horizontalalignment='left', verticalalignment='top', transform=ax.transAxes, fontsize=10) # ma='left'
    
    """Visualize APs (second axis)"""
    df_P['napex_sele_natural'] = np.where(df_P['napex_sele_natural']==1, df_P['napex_sele_natural'], np.nan)
    df_P['papex_sele_natural'] = np.where(df_P['papex_sele_natural']==1, df_P['papex_sele_natural'], np.nan)
    df_P['dapex_sele_natural'] = np.where(df_P['dapex_sele_natural']==1, df_P['dapex_sele_natural'], np.nan)

    # LABELS_GRAIN_TYPE_BAR        = ['PP','DF','PPgp','SH','DH','FC(xr)','RG','MF','MFcr','IF']
    # COLORS_GRAIN_TYPE_BAR_IACS2  = ['#00FF00','#228B22','#696969','#FF00FF','#0000FF','#ADD8E6','#FFB6C1','#FF0000','#FF0000','#00FFFF']
    COLORS_APS = {'newSnow':'#00FF00','windSlab':'#228B22','deepPW':'#0000FF','PW':'#ADD8E6','wetSnow':'#FF0000'}
    LW_NATURAL = 1.25
    C_NATURAL  = "orange" # "gold"
    ax_aps.set_ylim([0,6])

    ax_aps.bar(df_P['dy'], df_P['napex_sele_trigger'], width=0.75, bottom=5, align='edge', color=COLORS_APS['newSnow'])
    ax_aps.bar(df_P['dy'], df_P['winex'], width=0.75, bottom=4, align='edge', color=COLORS_APS['windSlab'])
    ax_aps.bar(df_P['dy'], df_P['papex_sele_trigger'], width=0.75, bottom=3, align='edge', color=COLORS_APS['PW'])
    ax_aps.bar(df_P['dy'], df_P['dapex_sele_trigger'], width=0.75, bottom=2, align='edge', color=COLORS_APS['deepPW'])
    ax_aps.bar(df_P['dy'], df_P['wapex_sele'], width=0.75, bottom=1, align='edge', color=COLORS_APS['wetSnow'])

    ax_aps.bar(df_P['dy'], df_P['napex_sele_natural'], width=0.75, bottom=5, align='edge', color=COLORS_APS['newSnow'], edgecolor = C_NATURAL, linewidth=LW_NATURAL)
    ax_aps.bar(df_P['dy'], df_P['papex_sele_natural'], width=0.75, bottom=3, align='edge', color=COLORS_APS['PW'], edgecolor = C_NATURAL, linewidth=LW_NATURAL)
    ax_aps.bar(df_P['dy'], df_P['dapex_sele_natural'], width=0.75, bottom=2, align='edge', color=COLORS_APS['deepPW'], edgecolor = C_NATURAL, linewidth=LW_NATURAL)
    ax_aps.bar(df_P['dy'], np.nan*df_P['papex_sele_natural'], width=0.75, bottom=0, align='edge', color="white", edgecolor = C_NATURAL, linewidth=LW_NATURAL, label="Natural release")

    ax_aps.legend(loc='upper left')
    ### Add icons of avalanche problems
    icon_xpos = -0.03
    icon_ypos = np.linspace(0.085,0.915,6)[::-1]
    shade = 0.75
    pad = 0.08
    icon_zoom = 0.041
    
    for i,icon in enumerate(icon_list):
        imagebox = OffsetImage(icon, zoom=icon_zoom)
        ab = AnnotationBbox(imagebox, (icon_xpos,icon_ypos[i]), xycoords="axes fraction", frameon=True, pad=pad)
        ax_aps.add_artist(ab)
    ax_aps.yaxis.set_ticklabels([])

    fig.tight_layout()
    fig.savefig(output_path, facecolor='w', edgecolor='w',
                format='png', dpi=250, bbox_inches='tight')
    plt.close(fig)
    print('[i]  Visualization of APs and snowpack evolution completed in {}s'.format(int(time.time()-start_time)))